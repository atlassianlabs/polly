FROM node:slim

# Install dependencies
RUN apt-get update -y
RUN apt-get install -y python2.7 python-pip libpq-dev

COPY . /opt/service
WORKDIR /opt/service
RUN npm install

EXPOSE 8080

CMD npm start