# Polly
### Polls in HipChat
An Atlassian Connect Add-on created using Atlassian Connect Express.

## Developing
Read ACE [docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies) for generic getting-started instructions.

## Run Polly in a Docker container

This is an experimental way for you to run Polly yourself using Docker, which
could be useful for "Behind the Firewall" (on-premises) Hipchat Data Center
deployments. The Dockerfile and Docker Compose file help you quickly download
and bundle any needed dependencies.

If you'll be running this on a deployment without access to the internet, your
process will look something like this:

1. Clone this repository.
2. Build a container from the repository.
3. Pass the container to your on-premises deployment using whatever process is approved by your organization.
4. Run the container on a host within your firewall. 
   Optionally, you may also push the image to a Docker Registry, Docker Trusted Registry, or Docker Datacenter repository. 

### Setup ###
1. Check for port conflicts on port 3023. 
   Polly will use this port, so you want it to be available. If you have a conflict, change the PORT variable in the `docker-compose` file to something else.
2. Clone this source repository to your local machine:
   `git clone https://bitbucket.org/atlassianlabs/polly.git`
   
### Build the container ###
1. Change directories to the source you just cloned:
   `cd polly`
2. Run the following command to use the Dockerfile in this repo to build a container with the latest version of the Polly bot.
   `sudo docker build -t atlassianlabs/polly:latest .`

### Run the Polly service ###
1. Run the following command to use the Docker Compose file in this repo to build the Polly service. 
   `docker-compose up -d`
  
3. Check the logs to make sure everything went smoothly using the following command. 
   `docker-compose logs`

4. Verify that the following URL returns a valid `capabilities.json` response. (Replace 'your-docker-host-fqdn' with your actual host.)
   `http://your-docker-host-fqdn:3023/`

### Install Polly on your Hipchat instance###
Next, make your Dockerized version of Polly bot available on your Hipchat service. 

> **Note:** You must be at least a room admin to install an integration in a room. Only admins can install integrations globally.

1. Log in to your Hipchat instance.
   1. If you're using Hipchat Cloud or Hipchat Server, click the **Manage** tab.
   2. If you're using Hipchat Data Center, log in to the web portal and click **Add-ons** in the left navigation.
2. Click **Install an add-on from a descriptor URL**. (It's at the very bottom of the page.)
3. In the dialog that appears, enter the URL you used above:
   `http://your-docker-host-fqdn:3023/`
   Hipchat verifies the add-on capabilities, and adds it to your deployment.

### Test ###
Go to a chat room and type `/polly`!

## Data Security and Privacy

In order to provide its services, Polly saves configuration data:

* Poll titles and options entered by you (could be sensitive, depending on what you enter)
* Poll configuration options entered by you (non sensitive)
* Your company's HipChat URL
* System-generated authentication keys

Chat room content and user details are *not* stored by Polly. However, messages
containing slash commands intended for Polly do pass through the add-on
temporarily. Polly does not store these.

Data is stored in Postgres databases in Amazon's cloud offering, protected by
username/password credentials. No data is sent to services apart from Polly,
HipChat and the infrastructure used by Polly (currently Amazon).

All communications between Polly and HipChat use HTTPS to prevent eavesdropping.

We have applied conscientious thought to protecting your data security and
privacy, with the genuine intention of
["DFTC"](https://www.atlassian.com/company/about/values). We will continue to
make our best efforts in this area.

However, this add-on is officially unsupported. You use it at your own risk. We
guarantee *nothing*. We intend to fix any serious problems ASAP, but for a free
add-on with no official support you must acknowledge that "best effort" support
provided for free means a lower level of support than for an official product.
There may be bugs of which we are currently unaware and we *may* not fix them
immediately upon becoming aware.

## Legal information

The library is licensed under the Apache License, Version 2.0 - see [LICENSE.txt](./LICENSE.txt)

To contribute if you are an Atlassian customer no further action is required
because our [End User Agreement](http://www.atlassian.com/end-user-agreement/)
(Section 7) gives Atlassian the right to use contributions from customers.  If
your are not an Atlassian customer then you will need to sign and submit our
[Contribution Agreement](ACLA.pdf).

## How to contribute

To contribute you code to Polly please:

* create a feature branch, 
* change the code, 
* add unit and/or integration tests, 
* test it, and 
* create a [pull request](https://bitbucket.org/atlassianlabs/polly/pull-requests).

After our review, your feature branch will be merged into the master branch and an Atlassian will release a new version of the service!

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the
appropriate link below to digitally sign the CLA. The Corporate CLA is for those
who are contributing as a member of an organization and the individual CLA is
for those contributing as an individual.

See _Contributors License Agreement_ section of [Open Source at Atlassian](https://developer.atlassian.com/opensource/) page.